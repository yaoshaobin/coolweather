package com.coolweather.app.util;

/**
 * Created by lenovo on 2016/8/13.
 */
public interface HttpCallbackListener {
    void onFinish(String response);
    void onError(Exception e);
}
